+++
weight = 2
title = "Useful For Everyone"
type = "features-usercases"
navsection = "features"
right = false
linktext = "Find out more"
+++

Manjaro is an efficient companion to serve your variety of needs and it is versatile to adapt to all of your needs. Our user base varies from home users to students & teachers, programmers & developers, professionals, art enthusiast, music lovers, professional multimedia professionals and many more.

We asked our users why they use Manjaro and what software they use to relax, learn or do their work. You can find here a incomplete list of software that is in most cases, is rapidly available via [the repositories](/features/tools#pamac) without hunting them down in the internet and keeping watch that you won't get malware.
