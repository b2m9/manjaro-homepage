+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-kde-17.1.12-stable-x86_64.iso"
Download_x64_Checksum = "01cfeb209d7eb6be0849791c4344145e91164898"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-kde-17.1.12-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-kde-17.1.12-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "KDE Edition"
Screenshot = "kde-full.jpg"
ShortDescription = "This edition is supported by the Manjaro team and comes with KDE, a very modern and flexible desktop."
Tags = [ "official", "beginnerfriendly", "traditional_ui" ]
TargetGroup = "For people who want a modern and very flexible desktop"
Thumbnail = "kde.jpg"
Version = "17.1.12"
date = "2018-08-18T17:36:00+02:00"
title = "Manjaro KDE Edition"
type="download-edition"
weight = 1
+++

KDE is a feature-rich and versatile desktop environment that provides several different styles of menu to access applications. An excellent built-in interface to easily access and install new themes, widgets, etc, from the internet is also worth mentioning. While very user-friendly and certainly flashy, KDE is also quite resource heavy and noticably slower to start and use than a desktop environment such as XFCE. A 64 bit installation of Manjaro running KDE uses about 550MB of memory.
