+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-xfce-17.1.12-stable-x86_64.iso"
Download_x64_Checksum = "9d59b84c700f88d6c0959539667f63384e925aed"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-xfce-17.1.12-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-xfce-17.1.12-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "XFCE Edition"
Screenshot = "xfce-full.jpg"
ShortDescription = "This edition is supported by the Manjaro team and comes with XFCE, a lightweight and reliable desktop with high configurability."
Tags = [ "official", "resourceefficient", "beginnerfriendly", "traditional_ui" ]
TargetGroup = "For people who want a reliable and fast desktop"
Thumbnail = "xfce.jpg"
Version = "17.1.12"
date = "2018-08-18T18:17:00+02:00"
title = "Manjaro XFCE Edition"
type="download-edition"
weight = 0
+++

Xfce is a lightweight desktop environment for UNIX-like operating systems. It aims to be fast and low on system resources, while still being visually appealing and user friendly. Xfce embodies the traditional UNIX philosophy of modularity and re-usability. It consists of a number of components that provide the full functionality one can expect of a modern desktop environment. They are packaged separately and you can pick among the available packages to create the optimal personal working environment.

