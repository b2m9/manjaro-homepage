+++
type="download-how-to-try"
+++

<!--- ## Try it now
--- --->
You can try Manjaro without changing your running system by putting it on a USB stick or on a DVD. If you decide to install Manjaro, you can do it directly from this live-system.

|   |   |
|---|---|
| <img src="/img/try/virtual-machine.svg" class="icon"> | Try Manjaro in a [virtual machine](/support/firststeps#branching-paths) within your current operating system  |
| <img src="/img/try/live-boot.svg" class="icon"> | Try Manjaro by [running it](/support/firststeps#branching-paths) from a DVD or USB-Stick  |
| <img src="/img/try/virtual-machine.svg" class="icon"> | [Install](/support/firststeps#branching-paths) Manjaro  from a DVD or USB-Stick  |
