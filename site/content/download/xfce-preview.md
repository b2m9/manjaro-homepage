+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-xfce-18.0-rc-1-stable-x86_64.iso"
Download_x64_Checksum = "579b38f38f8660f1df2d35b8ff9b03c627061f01"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-xfce-18.0-rc-1-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-xfce-18.0-rc-1-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "XFCE Edition (Developer Preview)"
Screenshot = "xfce-full.jpg"
ShortDescription = "This edition is supported by the Manjaro team and comes with XFCE, a lightweight and reliable desktop with high configurability. This preview is not suitable for production environment."
Tags = [ "official", "resourceefficient", "traditional_ui", "preview" ]
TargetGroup = "For testers and developers"
Thumbnail = "xfce-preview.jpg"
Version = "18.0-rc-1"
date = "2018-10-09T00:16:00+02:00"
title = "Manjaro XFCE Edition (Developer Preview)"
type="download-edition"
weight = 0
+++

**Note: This is a preview version for developers and testers and is not suitable for production environment as it may contain bugs**

Xfce is a lightweight desktop environment for UNIX-like operating systems. It aims to be fast and low on system resources, while still being visually appealing and user friendly. Xfce embodies the traditional UNIX philosophy of modularity and re-usability. It consists of a number of components that provide the full functionality one can expect of a modern desktop environment. They are packaged separately and you can pick among the available packages to create the optimal personal working environment.
