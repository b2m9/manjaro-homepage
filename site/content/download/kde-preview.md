+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-kde-18.0-rc-1-stable-x86_64.iso"
Download_x64_Checksum = "a993665f2945f65379727f230a16ba25b30ab3bc"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-kde-18.0-rc-1-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-kde-18.0-rc-1-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "KDE Edition (Developer Preview)"
Screenshot = "kde-full.jpg"
ShortDescription = "This edition is supported by the Manjaro team and comes with KDE, a very modern and flexible desktop. This preview is not suitable for production environment."
Tags = [ "official", "traditional_ui", "preview" ]
TargetGroup = "For testers and developers"
Thumbnail = "kde-preview.jpg"
Version = "18.0-rc-1"
date = "2018-10-09T00:16:00+02:00"
title = "Manjaro KDE Edition (Developer Preview)"
type="download-edition"
weight = 1
+++

**Note: This is a preview version for developers and testers and is not suitable for production environment as it may contain bugs**

KDE is a feature-rich and versatile desktop environment that provides several different styles of menu to access applications. An excellent built-in interface to easily access and install new themes, widgets, etc, from the internet is also worth mentioning. While very user-friendly and certainly flashy, KDE is also quite resource heavy and noticably slower to start and use than a desktop environment such as XFCE. A 64 bit installation of Manjaro running KDE uses about 550MB of memory.
