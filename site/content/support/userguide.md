+++
date = "2016-09-05T21:03:22+02:00"
title = "User Guide"
type = "support-post"
weight = 0
right = true
navsection = "support"
+++

<a href="https://de.osdn.net/projects/manjaro/storage/Manjaro-User-Guide.pdf" class="btn btn-success btn-xl" >Download User Guide in English</a>
<a href="https://de.osdn.net/projects/manjaro/storage/Manjaro-User-Guide-French.pdf" class="btn btn-success btn-xl" >Download User Guide in French</a>

Our team and our community proudly present you the Manjaro User Guide, a over 100 page long book with everything new and advanced users can know about Manjaro. We cover following topics in our guide:

|   |   |
|---|---|
| ![Icon](/img/actions/download.svg) | **Preparation**  |
|   | • Getting Manjaro |
|   | • Downloading Manjaro |
|   | • Checking a downloaded disc image for errors |
|   | • Writing a disc image |
| ![Icon](/img/actions/install.svg) | **Installation** |
|   | • Installing Manjaro |
|   | • Booting the Live environment |
|   | • Assisted installation on a BIOS system with Calamares |
|   | • Manual installation on a BIOS system with Calamares |
|   | • Assisted installation on a UEFI system with Calamares |
|   | • Manual installation on a UEFI system with Calamares |
|   | • Dual-booting with Microsoft Windows 10 |
|   | • Advanced installation methods |
| ![Icon](/img/try/install.svg) | **Using Manjaro** |
|   | • Welcome to Manjaro |
|   | • The Manjaro desktop |
|   | • Getting help |
|   | • Maintaining your system |
