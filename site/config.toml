theme = "manjaro-homepage"
baseurl = "/"
languageCode = "en-us"
title = "Manjaro - enjoy the simplicity"
googleAnalytics = ""
disqusShortname = ""
ignoreFiles = ["\\.Rmd$", "_files$", "_cache$"]
#enableGitInfo = true
publishDir = "../docs"
themesDir = "../.."
assetDir = "../static/"

[permalinks]
    post = "/:year/:month/:day/:slug/"


[[menu.main]]
    name = "Download"
    url = "/download/"
    pre = ""
    weight = 1


[[menu.main]]
    name = "Support"
    url = "#"
    pre = ""
    weight = 2
    identifier = "support"


[[menu.main]]
    name = "First Steps"
    url = "/support/firststeps/"
    pre = "<i class='now-ui-icons sport_user-run'></i>"
    weight = 1
    parent = "support"


[[menu.main]]
    name = "Common Problems"
    url = "/support/commonproblems/"
    pre = "<i class='now-ui-icons health_ambulance'></i>"
    weight = 2
    parent = "support"


[[menu.main]]
    name = "User Guide"
    url = "/support/userguide/"
    pre = "<i class='now-ui-icons education_agenda-bookmark'></i>"
    weight = 3
    parent = "support"


[[menu.main]]
    name = "Wiki"
    url = "https://wiki.manjaro.org"
    pre = "<i class='now-ui-icons location_map-big'></i>"
    weight = 4
    parent = "support"


[[menu.main]]
    name = "Forum"
    url = "https://forum.manjaro.org/"
    pre = ""
    weight = 3


[[menu.main]]
    name = "Packages"
    url = "https://repo.manjaro.org/"
    pre = ""
    weight = 2
    identifier = "packages"


[[menu.main]]
    name = "Mirrors Check"
    url = "https://repo.manjaro.org/"
    pre = "<i class='now-ui-icons objects_globe'></i>"
    weight = 1
    parent = "packages"


[[menu.main]]
    name = "Search Stable Branch"
    url = "https://repology.org/metapackages/?inrepo=manjaro_stable"
    pre = "<i class='now-ui-icons ui-1_zoom-bold'></i>"
    weight = 2
    parent = "packages"


[[menu.main]]
    name = "Search Testing Branch"
    url = "https://repology.org/metapackages/?inrepo=manjaro_testing"
    pre = "<i class='now-ui-icons ui-1_zoom-bold'></i>"
    weight = 3
    parent = "packages"


[[menu.main]]
    name = "Search Unstable Branch"
    url = "https://repology.org/metapackages/?inrepo=manjaro_stable"
    pre = "<i class='now-ui-icons ui-1_zoom-bold'></i>"
    weight = 4
    parent = "packages"


#[[menu.main]]
#    name = "Team"
#    url = "/profile/"
#    pre = "<i class='now-ui-icons users_single-02'></i>"
#    weight = 2


[[menu.main]]
    name = "Lists"
    url = "#"
    pre = ""
    weight = 4
    identifier = "mailinglists"


[[menu.main]]
    name = "Security"
    url = "https://lists.manjaro.org/mailman/listinfo/manjaro-security"
    pre = "<i class='now-ui-icons ui-1_email-85'></i>"
    weight = 1
    parent = "mailinglists"


[[menu.main]]
    name = "General"
    url = "https://lists.manjaro.org/mailman/listinfo/manjaro-general"
    pre = "<i class='now-ui-icons ui-1_email-85'></i>"
    weight = 2
    parent = "mailinglists"


[[menu.main]]
    name = "Packages"
    url = "https://lists.manjaro.org/mailman/listinfo/manjaro-packages"
    pre = "<i class='now-ui-icons ui-1_email-85'></i>"
    weight = 3
    parent = "mailinglists"


[[menu.main]]
    name = "Testing"
    url = "https://lists.manjaro.org/mailman/listinfo/manjaro-testing"
    pre = "<i class='now-ui-icons ui-1_email-85'></i>"
    weight = 4
    parent = "mailinglists"


[[menu.main]]
    name = "Development"
    url = "https://lists.manjaro.org/mailman/listinfo/manjaro-dev"
    pre = "<i class='now-ui-icons ui-1_email-85'></i>"
    weight = 5
    parent = "mailinglists"
    

[[menu.main]]
    name = "Mirrors"
    url = "https://lists.manjaro.org/mailman/listinfo/manjaro-mirrors"
    pre = "<i class='now-ui-icons ui-1_email-85'></i>"
    weight = 6
    parent = "mailinglists"


[[menu.main]]
    name = "About"
    url = "#"
    pre = ""
    weight = 4
    identifier = "features"


[[menu.main]]
    name = "What is Manjaro"
    url = "/what-is-manjaro/"
    pre = "<i class='now-ui-icons business_bulb-63'></i>"
    weight = 1
    parent = "features"


[[menu.main]]
    name = "Under your control"
    url = "/features/under-your-control/"
    pre = "<i class='now-ui-icons objects_spaceship'></i>"
    weight = 2
    parent = "features"


[[menu.main]]
    name = "Configured with one click"
    url = "/features/configured-with-one-click/"
    pre = "<i class='now-ui-icons ui-2_settings-90'></i>"
    weight = 3
    parent = "features"


[[menu.main]]
    name = "Useful for everyone"
    url = "/features/useful-for-everyone/"
    pre = "<i class='now-ui-icons ui-2_like'></i>"
    weight = 4
    parent = "features"


[[menu.main]]
    name = "Fresh & Stable"
    url = "/features/fresh-and-stable/"
    pre = "<i class='now-ui-icons ui-1_send'></i>"
    weight = 5
    parent = "features"


[[menu.main]]
    name = "Donate"
    pre = ""
    url = "/donate/"
    weight = 5 


[[menu.footer]]
    identifier = "twitter"
    pre = "<i class='fa fa-twitter'></i>"
    url = "https://twitter.com/manjarolinux"
    weight = 1


[[menu.footer]]
    identifier = "gitlab"
    pre = "<i class='fa fa-gitlab'></i>"
    url = "https://gitlab.manjaro.org"
    weight = 2


[[menu.footer]]
    identifier = "copyright"
    name = "2018 Manjaro Linux"
    pre = "<i class='fa fa-copyright'></i>"
    url = "https://spdx.org/licenses/MIT"
    weight = 3


[params]
    background = "/img/header.jpg"
    tint = "green"
    custom_css = ["css/highlight.min.css"]
    custom_js = []
    date_format = "Mon, Jan 2, 2006"
    description = "A website built through Hugo and blogdown."

    # options for highlight.js (version, additional languages, and theme)
    highlightjsVersion = "9.10.0"
    highlightCDN = "//cdn.bootcss.com"
    highlightjsLang = ["r", "yaml"]
    highlightjsTheme = "github"


[params.donate]
    headline = "Help us"
    description = "Besides participating in discussions to improve Manjaro, helping other users or help developing new functions, you can always donate money to help keeping our infrastructure funded. Just a few bucks can help to keep our website and forums ad-free, reliable and secure."
    conditions = "A donation counts as a private donation and is kept anonymous by us. That means that we won't share or publish any details about you and your donation. Donators get no rights or privileges - everybody can participate, whether they donated or not. Manjaro is still relatively young and a growing distribution - we are no registered organization. That means that we cannot give you a reciept that deduct taxes. For the same reason, the donation cannot go to a 'Manjaro Linux' organization, but goes to the head of the Manjaro team."

    [params.donate.account]
        account_holder = "Hanns Philip Müller"
        account_number = "1050676905"
        bank_identification = "12030000"
        bank = "DKB"
        bic = "BYLADEM1001"
        iban = "DE90120300001050676905"
        purpose = "Donation to Manjaro Linux"
        note = "On the transfer form the word donation must be mentioned in the purpose area. Who also indicates his email address, receives on request a confirmation of receipt."


[params.download_edition]
    download_x64_title = "64 bit version"
    download_x86_title = "32 bit version"
    download_x64_button = "Download 64 bit version"
    download_x86_button = "Download 32 bit version"
    download_x64_description = "Download the 64 bit version if you use a modern computer and the the maximum performance and full benefits of your modern hardware."
    download_x86_description = "Download the 32 bit version if you use an older computer or if you are unsure if your computer supports the 64 bit version."
    download_gpg = "Download GPG signature"
    download_torrent = "Download Torrent"
